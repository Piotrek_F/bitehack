package com.rosseta.bitehacke_2_0.ui.notifications;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.rosseta.bitehacke_2_0.R;

import java.sql.Date;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Random;

public class NotificationsFragment extends Fragment {

    Button button;
    private NotificationsViewModel notificationsViewModel;
    Handler handler=new Handler();
    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L ;
    int Seconds, Minutes, MilliSeconds ;
    long randomMili=0L;
    Random random=new Random();
    long milistart=0L;
    long secondsStart=0L;
    int test=0;
    boolean dupa = true;
    LocalDateTime date;

    public Runnable runnable = new Runnable() {

        public void run() {

            MillisecondTime = SystemClock.uptimeMillis() - StartTime;

            UpdateTime = TimeBuff + MillisecondTime;

            Seconds = (int) (UpdateTime / 1000);

            Minutes = Seconds / 60;

            Seconds = Seconds % 60;

            MilliSeconds = (int) (UpdateTime % 1000);



//            textView.setText("" + Minutes + ":"
//                    + String.format("%02d", Seconds) + ":"
//                    + String.format("%03d", MilliSeconds));

            if((Seconds * 1000 +  MilliSeconds) > randomMili  && dupa){
                dupa = false;
                button.setBackground(null);
                button.setBackgroundColor(getResources().getColor(R.color.green));
                milistart=MilliSeconds;
                secondsStart=Seconds;
                test=1;
                date = LocalDateTime.now();
            }

            handler.postDelayed(this, 0);
        }

    };

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                ViewModelProviders.of(this).get(NotificationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);
        button=root.findViewById(R.id.button3);
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(test!=1){
                if(button.getText().equals("START TEST")){
                    button.setText("");
                    startTimer();
                }}else{
                    stopTimer();
                }
            }
        });

        return root;
    }

    private void stopTimer() {
        handler.removeCallbacks(runnable);
        long diff = (secondsStart - Seconds)  *1000 + milistart - MilliSeconds;
        LocalDateTime now = LocalDateTime.now();
        Duration between = Duration.between(date, now);
        long seconds = between.getSeconds();
        long nano = between.getNano();
        long result = seconds * 1000 + (long)(nano / 1000000);
        button.setText(Long.toString(result));

    }

    private void startTimer() {
//        randomMili=random.nextInt(100)%4+2*500+random.nextInt(123);
        randomMili=random.nextInt(1000) * 3+1000;
        StartTime = SystemClock.uptimeMillis();
        handler.postDelayed(runnable, 10);
    }


}