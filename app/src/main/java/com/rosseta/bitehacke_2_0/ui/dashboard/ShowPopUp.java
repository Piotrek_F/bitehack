package com.rosseta.bitehacke_2_0.ui.dashboard;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;

import com.rosseta.bitehacke_2_0.R;

public class ShowPopUp extends Activity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_popup);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        getWindow().setLayout((int) (width * 0.4), (int)(height * 0.4));
    }
}