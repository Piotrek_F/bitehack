package com.rosseta.bitehacke_2_0.ui.home;

import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.rosseta.bitehacke_2_0.LocalDB;
import com.rosseta.bitehacke_2_0.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import in.goodiebag.carouselpicker.CarouselPicker;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    TextView timer;
    Button button;
    CarouselPicker carouselPicker1;
    int selectedProject=0;
    TextView textView;

    Handler handler=new Handler();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        Context context = root.getContext();
        textView=root.findViewById(R.id.textView);
        carouselPicker1=root.findViewById(R.id.carouselPicker1);
        List<CarouselPicker.PickerItem> itemsImages = new ArrayList<>();
        CarouselPicker.DrawableItem c1 = new CarouselPicker.DrawableItem(R.mipmap.ic_launcher);
        carouselPicker1.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(button.getText().equals("START")){
                    selectedProject=position;
                    timer.setText("" + LocalDB.project.get(selectedProject).getMinutes()+ ":"
                            + String.format("%02d", LocalDB.project.get(selectedProject).getSeconds()));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        itemsImages.add(new CarouselPicker.DrawableItem(R.mipmap.runningicon_foreground));
        itemsImages.add(new CarouselPicker.DrawableItem(R.mipmap.learningicon_foreground));
        itemsImages.add(new CarouselPicker.DrawableItem(R.mipmap.workingicon_foreground));
        CarouselPicker.CarouselViewAdapter imageAdapter = new CarouselPicker.CarouselViewAdapter(context,itemsImages,0);
        timer=root.findViewById(R.id.time);
        carouselPicker1.setAdapter(imageAdapter);
        button=root.findViewById(R.id.start);
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(button.getText().equals("START")){
                    button.setText("STOP");
                    startTimer();
                }else{
                    button.setText("START");
                    stopTimer();
                }
            }
        });
        return root;
    }

    public void startTimer(){
        LocalDB.project.get(selectedProject).setStartTime(SystemClock.uptimeMillis());
        handler.postDelayed(runnable, 0);
    }

    public void stopTimer(){
        Long tmp = LocalDB.project.get(selectedProject).getTimeBuff()+LocalDB.project.get(selectedProject).getMillisecondTime();
        LocalDB.project.get(selectedProject).setTimeBuff(tmp);
        handler.removeCallbacks(runnable);
    }

    public Runnable runnable = new Runnable() {

        public void run() {

            LocalDB.project.get(selectedProject).setMillisecondTime(SystemClock.uptimeMillis()-LocalDB.project.get(selectedProject).getStartTime());

            LocalDB.project.get(selectedProject).setUpdateTime(LocalDB.project.get(selectedProject).getTimeBuff()+LocalDB.project.get(selectedProject).getMillisecondTime());

            LocalDB.project.get(selectedProject).setSeconds((int)LocalDB.project.get(selectedProject).getUpdateTime()/1000);

            LocalDB.project.get(selectedProject).setMinutes(LocalDB.project.get(selectedProject).getSeconds()/60);

            LocalDB.project.get(selectedProject).setSeconds(LocalDB.project.get(selectedProject).getSeconds()%60);

            LocalDB.project.get(selectedProject).setMilliSeconds((int)LocalDB.project.get(selectedProject).getUpdateTime()%1000);

            timer.setText("" + LocalDB.project.get(selectedProject).getMinutes()+ ":"
                    + String.format("%02d", LocalDB.project.get(selectedProject).getSeconds()));

            if(LocalDB.project.get(selectedProject).getSeconds()%60==0){
                LocalDB.summaryTime=LocalDB.summaryTime+1;
                if(LocalDB.wf==1){
                    LocalDB.workTimer=LocalDB.workTimer+1;
                }else{
                    LocalDB.freeTimer=LocalDB.freeTimer+1;
                }

            }

             /*
             LocalDB.freeTimer - licznik czasu wolnego
             LocalDb.breakTime - przy ilu minutach powinien mieć przerwe
             LocalDB.wf - 0 gdy odpoczywa; 1 gdy pracuje
             */
            if(shouldStartWorking()){
                startWork();
            }else if(shouldStartResting()){
                startRest();
            }
            handler.postDelayed(this, 1000);
        }

        private void startRest() {
            timer.setTextColor(Color.parseColor("#CCFF00"));
            textView.setText("REST");
            LocalDB.workTimer=0;
            LocalDB.wf=0;
            MediaPlayer mPlayer = MediaPlayer.create(getContext(), R.raw.crystalbungalow);
            mPlayer.start();

        }

        private void startWork() {
            timer.setTextColor(Color.parseColor("#81D8D0"));
            textView.setText("WORK");
            LocalDB.freeTimer=0;
            LocalDB.wf=1;
            LocalDB.iteration=1;
            MediaPlayer mPlayer = MediaPlayer.create(getContext(), R.raw.crystalbungalow);
            mPlayer.start();
        }

        private boolean shouldStartResting() {
            return LocalDB.workTimer==LocalDB.workTime && LocalDB.wf==1;
        }

    };

    private boolean shouldStartWorking() {
        return (LocalDB.freeTimer==LocalDB.breakTime && LocalDB.wf==0) || LocalDB.iteration==0;
    }


}