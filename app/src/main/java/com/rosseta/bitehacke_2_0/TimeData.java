package com.rosseta.bitehacke_2_0;

public class TimeData {
    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L ;
    int Seconds, Minutes, MilliSeconds=0 ;

    public long getMillisecondTime() {
        return MillisecondTime;
    }

    public void setMillisecondTime(long millisecondTime) {
        MillisecondTime = millisecondTime;
    }

    public long getStartTime() {
        return StartTime;
    }

    public void setStartTime(long startTime) {
        StartTime = startTime;
    }

    public long getTimeBuff() {
        return TimeBuff;
    }

    public void setTimeBuff(long timeBuff) {
        TimeBuff = timeBuff;
    }

    public long getUpdateTime() {
        return UpdateTime;
    }

    public void setUpdateTime(long updateTime) {
        UpdateTime = updateTime;
    }

    public int getSeconds() {
        return Seconds;
    }

    public void setSeconds(int seconds) {
        Seconds = seconds;
    }

    public int getMinutes() {
        return Minutes;
    }

    public void setMinutes(int minutes) {
        Minutes = minutes;
    }

    public int getMilliSeconds() {
        return MilliSeconds;
    }

    public void setMilliSeconds(int milliSeconds) {
        MilliSeconds = milliSeconds;
    }
}
